import React from "react";

import "./Card.css";

const Card = (props) => (
    <>
        <div className="card cardGroup__card">
            <div className="card__description cardGroup__cardDescription">
                {props.icon && (
                    <div
                        className={
                            "icon fa " + props.icon + " card__descriptionIcon"
                        }
                    />
                )}
                {props.description && (
                    <div className="card__descriptionText">
                        {props.description}
                        <br />
                        {props.tip && (
                            <div className="card__descriptionTextTip">
                                {props.tip}
                            </div>
                        )}
                    </div>
                )}
            </div>
            {props.price && <div className="card__price">{props.price}</div>}
        </div>
    </>
);

export default Card;
