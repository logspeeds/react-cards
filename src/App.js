import React from "react";
import Card from "./Card.js";
import CardGroup from "./CardGroup.js";

const App = () => (
    <CardGroup>
        <Card icon="fa-thumbs-o-up" description="Trial" price="Free!" />
        <Card
            icon="fa-trophy"
            description="Basic tier"
            tip="(most popular)"
            price="$10.00"
        />
        <Card
            icon="fa-bolt"
            description="Advanced tier"
            tip="(only for enterprise-level professionals)"
            price="$6,000.00"
        />
    </CardGroup>
);

export default App;
